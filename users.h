#include <iostream>
#include <vector>
#include "user.h"

using namespace std;

class Users
{
    vector<User> users;

public:
    void addUser(const User &user);
    int getMaxUserId();
    void changePassword(int loggedUserId);
    void loadUsersFromTxtFile();
    int logon();
    User registration();
    int registrationAndLogin();
    void printLogonMenu();
};
