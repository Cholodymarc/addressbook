#include <iostream>

using namespace std;

class User
{
    int id;
    string login;
    string password;

public:
    User();
    User(int,string,string);
    int getId();
    string getLogin();
    string getPassword();
    void setId(int idd);
    void setLogin(string lgn);
    void setPassword(string pwd);
    void saveUserToTxtFile();
    void saveUserToTxtFileAfterUpdate();
};
