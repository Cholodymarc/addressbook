#include <iostream>
#include <vector>
#include <fstream>
#include <windows.h>
#include <stdio.h>
#include "users.h"

using namespace std;

struct ContactPerson
{
    int id;
    int userId;
    string name;
    string surname;
    string phone;
    string email;
    string address;
};

void printWelcomeMessage();
void printMenu();
void printMenuForUpdate();
void printAllContacts(const vector<ContactPerson> &contacts);
void printContactsByName(const vector<ContactPerson> &contacts);
void printContactsBySurname(const vector<ContactPerson> &contacts);
ContactPerson addContact(int contactId, int loggedUserId, vector<ContactPerson> &contacts);
void addContact(vector<ContactPerson> &contacts, const ContactPerson &ContactPerson);
void deleteContactById(vector<ContactPerson> &contacts, int id);
void updateContactById(vector<ContactPerson> &contacts, int id);
int getIdOfContactToUpdate();
int getIdOfContactToDelete();
char getContactDeletionConfirmation(int id);
int loadContactsFromTxtFile(int loggedUserId, vector<ContactPerson> &contacts);
void saveContactsToTxtFile(const vector<ContactPerson> &contacts);
void saveToTxtFileAfterAdd(const ContactPerson contactPerson);
void saveToTxtFileAfterDelete(const int idOfContactToDelete);
void saveToTxtFileAfterUpdate(ContactPerson contactPerson);

int main()
{
    char menuOption;
    Users users;
    vector<ContactPerson> contacts;
    int loggedUserId;
    int maxContactId;
    ContactPerson contactPerson;
    User registeredUser;
    int idOfContactToDelete;

    loggedUserId = users.registrationAndLogin();

    maxContactId = loadContactsFromTxtFile(loggedUserId, contacts);
    printWelcomeMessage();
    while(true)
    {
        printMenu();
        cout << endl << "Your selection: ";
        cin >> menuOption;
        system("cls");
        switch(menuOption)
        {
        case '1':
            contactPerson = addContact(++maxContactId, loggedUserId, contacts);
            saveToTxtFileAfterAdd(contactPerson);
            break;
        case '2':
            printContactsByName(contacts);
            break;
        case '3':
            printContactsBySurname(contacts);
            break;
        case '4':
            printAllContacts(contacts);
            break;
        case '5':
            printAllContacts(contacts);
            idOfContactToDelete = getIdOfContactToDelete();
            deleteContactById(contacts, idOfContactToDelete);
            contacts.clear();
            maxContactId = loadContactsFromTxtFile(loggedUserId, contacts);
            break;
        case '6':
            printAllContacts(contacts);
            updateContactById(contacts, getIdOfContactToUpdate());
            break;
        case '7':
            users.changePassword(loggedUserId);
            break;
        case '8':
            loggedUserId = users.registrationAndLogin();
            contacts.clear();
            maxContactId = loadContactsFromTxtFile(loggedUserId, contacts);
            break;
        case '9':
            exit(0);
            break;
        default :
            cout << "Please enter valid option 1-9: " << endl;
            cin.clear();
            cin.sync();
        }
    }
    return 0;
}

void printWelcomeMessage()
{
    cout << "Welcome in your Address Book!" << endl;
}

void printMenu()
{
    cout << "1. Add friend to the contact list" << endl;
    cout << "2. Find by name" << endl;
    cout << "3. Find by surname" << endl;
    cout << "4. Print all contacts" << endl;
    cout << "5. Delete contact" << endl;
    cout << "6. Update contact" << endl;
    cout << "7. Change password" << endl;
    cout << "8. Logout" << endl;
    cout << "9. Exit";
}

void printContactsByName(const vector<ContactPerson> &contacts)
{
    string searchedContactPersonName;
    cout << "Please enter name: " << endl;
    cin >> searchedContactPersonName;
    cout << "******************" << endl;
    cout << "Contact list: " << endl;
    for(vector<ContactPerson>::const_iterator itr = contacts.begin(), end = contacts.end(); itr != end; ++itr)
    {
        if(itr->name == searchedContactPersonName)
        {
            cout << "[Id: " << itr->id;
            cout << ", UserId: " << itr->userId;
            cout << ", Name: " << itr->name;
            cout << ", Surname: " << itr->surname;
            cout << ", Phone: " << itr->phone;
            cout << ", Email: " << itr->email;
            cout << ", Address: " << itr->address << "]" << endl;
        }
    }
    cout << endl;
}

void printContactsBySurname(const vector<ContactPerson> &contacts)
{
    string searchedContactPersonSurname;
    cout << "Please enter surname: " << endl;
    cin >> searchedContactPersonSurname;
    cout << "******************" << endl;
    cout << "Contact list: " << endl;
    for(vector<ContactPerson>::const_iterator itr = contacts.begin(), end = contacts.end(); itr != end; ++itr)
    {
        if(itr->surname == searchedContactPersonSurname)
        {
            cout << "[Id: " << itr->id;
            cout << ", UserId: " << itr->userId;
            cout << ", Name: " << itr->name;
            cout << ", Surname: " << itr->surname;
            cout << ", Phone: " << itr->phone;
            cout << ", Email: " << itr->email;
            cout << ", Address: " << itr->address << "]" << endl;
        }
    }
    cout << endl;
}

void printAllContacts(const vector<ContactPerson> &contacts)
{
    cout << "******************" << endl;
    cout << "Contact list: " << endl;
    for(vector<ContactPerson>::const_iterator itr = contacts.begin(), end = contacts.end(); itr != end; ++itr)
    {
        cout << "[Id: " << itr->id;
        cout << ", UserId: " << itr->userId;
        cout << ", Name: " << itr->name;
        cout << ", Surname: " << itr->surname;
        cout << ", Phone: " << itr->phone;
        cout << ", Email: " << itr->email;
        cout << ", Address: " << itr->address << "]" << endl;
    }
    cout << endl;
}

void saveToTxtFileAfterAdd(const ContactPerson contactPerson)
{
    fstream fileForOutput;
    fileForOutput.open("contacts.txt",ios::out | ios::app);
    fileForOutput << contactPerson.id << "|";
    fileForOutput << contactPerson.userId << "|";
    fileForOutput << contactPerson.name << "|";
    fileForOutput << contactPerson.surname << "|";
    fileForOutput << contactPerson.phone << "|";
    fileForOutput << contactPerson.email << "|";
    fileForOutput << contactPerson.address << "|" << endl;
    fileForOutput.close();
}

void saveToTxtFileAfterDelete(const int idOfContactToDelete)
{
    string idTxt;
    string line;
    fstream fileForInput;
    fstream fileForOutput;
    int id;
    fileForOutput.open("contacts_temp.txt",ios::out);
    fileForInput.open("contacts.txt",ios::in);

    while(getline(fileForInput, idTxt, '|'))
    {
        id = atoi(idTxt.c_str());
        getline(fileForInput, line, '\n');
        if(id == idOfContactToDelete)
        {
            continue;
        }
        fileForOutput << id << "|";
        fileForOutput << line << endl;
    }
    fileForInput.close();
    fileForOutput.close();
    if (remove("contacts.txt") != 0)
    {
        perror("File deletion failed");
    }
    rename("contacts_temp.txt", "contacts.txt");
}

void saveToTxtFileAfterUpdate(ContactPerson contactPerson)
{
    string idTxt;
    string line;
    fstream fileForInput;
    fstream fileForOutput;
    int id;
    fileForOutput.open("contacts_temp.txt",ios::out);
    fileForInput.open("contacts.txt",ios::in);

    while(getline(fileForInput, idTxt, '|'))
    {
        id = atoi(idTxt.c_str());
        getline(fileForInput, line, '\n');
        if(id == contactPerson.id)
        {
            fileForOutput << contactPerson.id << "|";
            fileForOutput << contactPerson.userId << "|";
            fileForOutput << contactPerson.name << "|";
            fileForOutput << contactPerson.surname << "|";
            fileForOutput << contactPerson.phone << "|";
            fileForOutput << contactPerson.email << "|";
            fileForOutput << contactPerson.address << "|" << endl;
            continue;
        }
        fileForOutput << id << "|";
        fileForOutput << line << endl;
    }
    fileForInput.close();
    fileForOutput.close();
    if (remove("contacts.txt") != 0)
    {
        perror("File deletion failed");
    }
    rename("contacts_temp.txt", "contacts.txt");
}

int loadContactsFromTxtFile(int loggedUserId, vector<ContactPerson> &contacts)
{
    ContactPerson contactPerson;
    string idTxt;
    string userIdTxt;
    string newLine;
    int maxId = 1;

    fstream fileForInput;
    fileForInput.open("contacts.txt",ios::in);
    if(fileForInput.good()==false) return 0;

    while(getline(fileForInput, idTxt, '|'))
    {
        contactPerson.id = atoi(idTxt.c_str());
        if(contactPerson.id > maxId)
        {
            maxId = contactPerson.id;
        }
        getline(fileForInput, userIdTxt, '|');
        contactPerson.userId = atoi(userIdTxt.c_str());
        getline(fileForInput, contactPerson.name, '|');
        getline(fileForInput, contactPerson.surname, '|');
        getline(fileForInput, contactPerson.phone, '|');
        getline(fileForInput, contactPerson.email, '|');
        getline(fileForInput, contactPerson.address, '|');
        getline(fileForInput, newLine, '\n');
        if(loggedUserId != contactPerson.userId)
        {
            continue;
        }
        addContact(contacts, contactPerson);
    }

    fileForInput.close();
    return maxId;
}

ContactPerson addContact(int contactId, int loggedUserId, vector<ContactPerson> &contacts)
{
    ContactPerson contactPerson;
    contactPerson.id = contactId;
    contactPerson.userId = loggedUserId;
    cin.clear();
    cin.sync();
    cout << "Please input your friend details";
    cout << endl << "Name: ";
    getline(cin, contactPerson.name);
    cout << "Surname: ";
    getline(cin, contactPerson.surname);
    cout << "Phone: ";
    getline(cin, contactPerson.phone);
    cout << "Email: ";
    getline(cin, contactPerson.email);
    cout << "Address: ";
    getline(cin, contactPerson.address);

    addContact(contacts, contactPerson);
    return contactPerson;
}

void addContact(vector<ContactPerson> &contacts, const ContactPerson &contactPerson)
{
    contacts.push_back(contactPerson);
}

int getIdOfContactToDelete()
{
    int idOfContactToDelete;
    cout << "Please provide id of the contact to delete: ";
    cin >> idOfContactToDelete;
    return idOfContactToDelete;
}

char getContactDeletionConfirmation(int id)
{
    char userDecision;
    cout << "Do you confirm removal of contact with id " << id << " [t/n]: ";
    cin >> userDecision;
    return userDecision;
}

void deleteContactById(vector<ContactPerson> &contacts, int id)
{

    for(vector<ContactPerson>::iterator itr = contacts.begin(), end = contacts.end(); itr != end; ++itr)
    {
        if(itr->id == id)
        {

            if('t' == getContactDeletionConfirmation(id))
            {
                contacts.erase(itr);
                cout << "Contact with id " << id << " removed!" << endl;
                saveToTxtFileAfterDelete(id);
            }
            else
            {
                cout << "No change in the contact list" << endl;
            }
            return;
        }
    }
    cout << "Contact with id " << id << " not found!" << endl;
}

int getIdOfContactToUpdate()
{
    int idOfContactToUpdate;
    cout << "Please provide id of the contact to update: ";
    cin >> idOfContactToUpdate;
    return idOfContactToUpdate;
}

void updateContactById(vector<ContactPerson> &contacts, int id)
{
    char selectedMenuUpdateOption;
    string userInput;
    ContactPerson contactPerson;

    for(vector<ContactPerson>::iterator itr = contacts.begin(), end = contacts.end(); itr != end; ++itr)
    {
        if(itr->id == id)
        {
            printMenuForUpdate();
            cin >> selectedMenuUpdateOption;
            switch(selectedMenuUpdateOption)
            {
            case '1':
                cout << "Enter new name: ";
                cin >> userInput;
                itr->name = userInput;
                break;
            case '2':
                cout << "Enter new surname: ";
                cin >> userInput;
                itr->surname = userInput;
                break;
            case '3':
                cout << "Enter new phone number: ";
                cin >> userInput;
                itr->phone = userInput;
                break;
            case '4':
                cout << "Enter new email: ";
                cin >> userInput;
                itr->email = userInput;
                break;
            case '5':
                cout << "Enter new address: ";
                cin >> userInput;
                itr->address = userInput;
                break;
            case '6':
                cout << "No update" << endl;
                break;
            default:
                cout << "Wrong selection!" << endl;
                break;
            }
            contactPerson.id = itr->id;
            contactPerson.userId = itr->userId;
            contactPerson.name = itr->name;
            contactPerson.surname = itr->surname;
            contactPerson.phone = itr->phone;
            contactPerson.email = itr->email;
            contactPerson.address = itr->address;
            saveToTxtFileAfterUpdate(contactPerson);
            return;
        }
    }
    cout << "There is no contact with id: " << id << endl;
}

void printMenuForUpdate()
{
    cout << "Which item would you like to update?" << endl;
    cout << "1. Name" << endl;
    cout << "2. Surname" << endl;
    cout << "3. Phone" << endl;
    cout << "4. Email" << endl;
    cout << "5. Address" << endl;
    cout << "6. Back to menu" << endl;
    cout << "Your selection: ";
}
