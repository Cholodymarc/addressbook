#include <iostream>
#include <fstream>
#include <vector>
#include "user.h"

using namespace std;

User::User(){}

User::User(int idd, string lgn, string pwd)
{
    id = idd;
    login = lgn;
    password = pwd;
}

int User::getId()
{
    return id;
}

string User::getLogin()
{
    return login;
}

string User::getPassword()
{
    return password;
}

void User::setId(int idd)
{
    id = idd;
}

void User::setLogin(string lgn)
{
    login = lgn;
}

void User::setPassword(string pwd)
{
    password = pwd;
}

void User::saveUserToTxtFile()
{
    fstream fileForOutput;
    fileForOutput.open("users.txt",ios::out | ios::app);
    fileForOutput << id << "|";
    fileForOutput << login << "|";
    fileForOutput << password << "|" << endl;
    fileForOutput.close();
}

void User::saveUserToTxtFileAfterUpdate()
{
    string idTxt;
    string line;
    fstream fileForInput;
    fstream fileForOutput;
    int userId;
    fileForOutput.open("users_temp.txt",ios::out);
    fileForInput.open("users.txt",ios::in);

    while(getline(fileForInput, idTxt, '|'))
    {
        userId = atoi(idTxt.c_str());
        getline(fileForInput, line, '\n');
        if(userId == id)
        {
            fileForOutput << id << "|";
            fileForOutput << login << "|";
            fileForOutput << password << "|" << endl;
            continue;
        }
        fileForOutput << userId << "|";
        fileForOutput << line << endl;
    }
    fileForInput.close();
    fileForOutput.close();
    if (remove("users.txt") != 0)
    {
        perror("File deletion failed");
    }
    rename("users_temp.txt", "users.txt");
}
