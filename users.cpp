#include <iostream>
#include <windows.h>
#include <fstream>

#include "users.h"

using namespace std;

void Users::addUser(const User &user)
{
    users.push_back(user);
}

int Users::getMaxUserId()
{
    int id = 0;
    if(users.size() > 0)
    {
        for(vector<User>::iterator itr = users.begin(), end = users.end(); itr != end; ++itr)
        {
            if(itr->getId() > id)
            {
                id = itr->getId();
            }
        }
    }
    return id;
}

void Users::changePassword(int loggedUserId)
{
    User user;
    string newPassword1;
    string newPassword2;
    for(vector<User>::iterator itr = users.begin(), end = users.end(); itr != end; ++itr)
    {
        if(itr->getId() == loggedUserId)
        {
            cout << "Please enter new password: ";
            cin >> newPassword1;
            cout << "Please confirm new password: ";
            cin >> newPassword2;
            if(newPassword1 == newPassword2)
            {
                itr->setPassword(newPassword1);
                user.setId(itr->getId());
                user.setLogin(itr->getLogin());
                user.setPassword(newPassword1);
                user.saveUserToTxtFileAfterUpdate();
                cout << "Password successfully changed!" << endl;
                Sleep(1500);
                return;
            }
            else
            {
                cout << "Incorrect input. Password not changed.";
                Sleep(1500);
            }
        }
    }
}

void Users::loadUsersFromTxtFile()
{
    User user;
    string idTxt;
    string newLine;
    string login;
    string password;

    fstream fileForInput;
    fileForInput.open("users.txt",ios::in);
    if(fileForInput.good()==false) return;

    while(getline(fileForInput, idTxt, '|'))
    {
        user.setId(atoi(idTxt.c_str()));
        getline(fileForInput, login, '|');
        getline(fileForInput, password, '|');
        getline(fileForInput, newLine, '\n');
        user.setLogin(login);
        user.setPassword(password);
        addUser(user);
    }

    fileForInput.close();

}

int Users::logon()
{
    string login;
    string password;
    cout << "Enter login: ";
    cin >> login;
    for(vector<User>::iterator itr = users.begin(), end = users.end(); itr != end; ++itr)
    {
        if(itr->getLogin() == login)
        {
            for(int attempts = 0; attempts < 3; attempts++)
            {
                cout << "Enter password. Attempts remaining " << 3-attempts << ": ";
                cin >> password;
                if(itr -> getPassword() == password)
                {
                    cout << "You are successfully logged in." << endl;
                    Sleep(1000);
                    return itr -> getId();
                }
            }
            cout << "You entered incorrect password three times. Please wait three seconds and try again.";
            Sleep(3000);
            return 0;
        }
    }
    cout << "User with provided login does not exist." << endl;
    Sleep(1500);
    return 0;
}

User Users::registration()
{
    User userToRegister;
    string login;
    string password;
    userToRegister.setId(getMaxUserId() + 1);
    cout << "Enter login: ";
    cin >> login;
    userToRegister.setLogin(login);

    for(vector<User>::iterator itr = users.begin(), end = users.end(); itr != end; ++itr)
    {
        if(itr->getLogin() == userToRegister.getLogin())
        {
            cout << "User with provided login already exists. Please enter another login: ";
            cin >> login;
            userToRegister.setLogin(login);
            itr = users.begin();
        }
    }

    cout << "Enter password: ";
    cin >> password;
    userToRegister.setPassword(password);
    cout << "Account setup properly." << endl;
    Sleep(1000);
    addUser(userToRegister);
    return userToRegister;
}

int Users::registrationAndLogin()
{
    char menuOption;
    int loggedUserId;
    User registeredUser;
    while(true)
    {
        loadUsersFromTxtFile();

        printLogonMenu();
        cin >> menuOption;

        if(menuOption == '1')
        {
            registeredUser = registration();
            registeredUser.saveUserToTxtFile();
        }
        else if (menuOption == '2')
        {
            loggedUserId = logon();
            if(loggedUserId != 0)
            {
                break;
            }
        }
        else if (menuOption == '9')
        {
            exit(0);
        }
    }
    system("cls");
    return loggedUserId;
}

void Users::printLogonMenu()
{
    system("cls");
    cout << "1. Registration" << endl;
    cout << "2. Login" << endl;
    cout << "9. Exit" << endl;
}
